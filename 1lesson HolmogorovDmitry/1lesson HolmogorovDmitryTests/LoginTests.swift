//
//  LoginTests.swift
//  1lesson HolmogorovDmitryTests
//
//  Created by Дмитрий on 25/04/2019.
//  Copyright © 2019 Dmitry. All rights reserved.
//

import XCTest
@testable import _lesson_HolmogorovDmitry

class LoginTests: XCTestCase {
    
    var requestFactory: RequestFactory!
    let username: String = "Somebody"
    let password: String = "mypassword"
    
    override func setUp() {
        super.setUp()
        requestFactory = RequestFactory()
    }

    override func tearDown() {
        super.tearDown()
        requestFactory = nil
    }

    func test_authLogin() {
        let auth = self.requestFactory.makeAuthRequestFatory()
        
        auth.login(userName: username, password: password) { response in
            
            assert(response.result.isSuccess, "данные получены")
            assert(response.result.isFailure, "ошибка")
            
            switch response.result {
            case .success:
                print("данные получены")
            case .failure:
                XCTFail()
            }
        }
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
