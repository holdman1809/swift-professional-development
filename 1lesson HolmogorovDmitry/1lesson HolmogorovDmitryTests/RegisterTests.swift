//
//  RegisterTests.swift
//  1lesson HolmogorovDmitryTests
//
//  Created by Дмитрий on 26/04/2019.
//  Copyright © 2019 Dmitry. All rights reserved.
//

import XCTest
@testable import _lesson_HolmogorovDmitry


class RegisterTests: XCTestCase {

    var requestFactory: RequestFactory!
    let username: String = "Somebody"
    let password: String = "mypassword"
    let id_user: Int = 123
    let email: String = "bondJames@quant.com"
    let gender: String = "m"
    let credit_card: String = "123-123"
    let bio: String = "My name is James..James Bond"
    
    override func setUp() {
        super.setUp()
        requestFactory = RequestFactory()
    }

    override func tearDown() {
        super.tearDown()
        requestFactory = nil
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func test_RegisterNewUser() {
        
        let register = self.requestFactory.makeRegisterRequestFatory()
        register.register(id_user: id_user, userName: username, password: password, email: email, gender: gender, credit_card: credit_card, bio: bio) { (response) in
            
            assert(response.result.isSuccess, "данные получены")
            assert(response.result.isFailure, "ошибка")
        }
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
