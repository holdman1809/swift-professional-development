//
//  _lesson_HolmogorovDmitryUITests.swift
//  1lesson HolmogorovDmitryUITests
//
//  Created by Дмитрий on 17/04/2019.
//  Copyright © 2019 Dmitry. All rights reserved.
//

import XCTest

class _lesson_HolmogorovDmitryUITests: XCTestCase {
    private var app: XCUIApplication!
    
    override func setUp() {
        super.setUp()
        continueAfterFailure = false
        app = XCUIApplication()
        app.launch()

    }

    override func tearDown() {
        super.tearDown()
    }

    func testSuccess() {
        let loginTextField = app.textFields["Логин"]
        loginTextField.tap()
        loginTextField.typeText("D")

        let passwordTextField = app.textFields["Пароль"]
        passwordTextField.tap()
        passwordTextField.typeText("D")
        
        let signInButton = app.buttons["Войти"]
        signInButton.tap()
        
        let resultLabel = app.staticTexts["Данные верны"]
        XCTAssertNotNil(resultLabel)
       
    }
    
    func testFail() {
        let loginTextField = app.textFields["Логин"]
        loginTextField.tap()
        loginTextField.typeText("111")
        
        let passwordTextField = app.textFields["Пароль"]
        passwordTextField.tap()
        passwordTextField.typeText("111")
        
        let signInButton = app.buttons["Войти"]
        signInButton.tap()
        
        let resultLabel = app.staticTexts["Данные верны"]
        XCTAssertNotNil(resultLabel)
    }
}
