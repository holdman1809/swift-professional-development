//
//  PaymentOrderRequestFactory.swift
//  1lesson HolmogorovDmitry
//
//  Created by Дмитрий on 10/05/2019.
//  Copyright © 2019 Dmitry. All rights reserved.
//

import Foundation
import Alamofire

protocol PaymentOrderRequestFactory {
    func payOrder(order: String, totalPrice: String, id_product: String, product_name: String, price: String, quantity: String,
                      completionHandler: @escaping (DataResponse<PaymentOrderResult>) -> Void)
}
