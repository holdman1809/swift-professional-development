//
//  PaymentOrder.swift
//  1lesson HolmogorovDmitry
//
//  Created by Дмитрий on 10/05/2019.
//  Copyright © 2019 Dmitry. All rights reserved.
//

import Foundation
import Alamofire

class PaymentOrder: AbstractRequestFactory {
    let errorParser: AbstractErrorParser
    let sessionManager: SessionManager
    let queue: DispatchQueue?
    let baseUrl = URL(string: "http://localhost:8080/")!

    init(errorParser: AbstractErrorParser,
         sessionManager: SessionManager,
         queue: DispatchQueue? = DispatchQueue.global(qos: .utility)) {
        
        self.errorParser = errorParser
        self.sessionManager = sessionManager
        self.queue = queue
    }
}

extension PaymentOrder: PaymentOrderRequestFactory {
    func payOrder(order: String, totalPrice: String, id_product: String, product_name: String, price: String, quantity: String, completionHandler: @escaping (DataResponse<PaymentOrderResult>) -> Void) {
        let requestModel = PayOrder(baseUrl: baseUrl, order: order, totalPrice: totalPrice, id_product: totalPrice, product_name: product_name, price: price, quantity: quantity)
        self.request(request: requestModel, completionHandler: completionHandler).session.finishTasksAndInvalidate()
    }
}

extension PaymentOrder {
    struct PayOrder: RequestRouter {
        let baseUrl: URL
        let method: HTTPMethod = .get
        let path: String = "paymentorder"
        
        let order: String
        let totalPrice: String
        let id_product: String
        let product_name: String
        let price: String
        let quantity: String
        
        var parameters: Parameters? {
            return [
                "order": order,
                "totalPrice": totalPrice,
                "id_product": id_product,
                "product_name": product_name,
                "price": price,
                "quantity": quantity
            ]
        }
    }
}
