//
//  ApproveReview.swift
//  1lesson HolmogorovDmitry
//
//  Created by Дмитрий on 06/05/2019.
//  Copyright © 2019 Dmitry. All rights reserved.
//

import Foundation
import Alamofire

class ApproveReview: AbstractRequestFactory {
    let errorParser: AbstractErrorParser
    let sessionManager: SessionManager
    let queue: DispatchQueue?
    let baseUrl = URL(string: "https://raw.githubusercontent.com/GeekBrainsTutorial/online-store-api/master/responses/")!
    
    init(errorParser: AbstractErrorParser,
         sessionManager: SessionManager,
         queue: DispatchQueue? = DispatchQueue.global(qos: .utility)) {
        
        self.errorParser = errorParser
        self.sessionManager = sessionManager
        self.queue = queue
    }
}

extension ApproveReview: ApproveReviewRequestFactory {
    func approveReview(id_comment: Int, completionHandler: @escaping (DataResponse<ApproveReviewResult>) -> Void) {
        let requestModel = Approve(baseUrl: baseUrl, id_comment: id_comment)
        self.request(request: requestModel, completionHandler: completionHandler).session.finishTasksAndInvalidate()
    }
}

extension ApproveReview {
    struct Approve: RequestRouter {
        let baseUrl: URL
        let method: HTTPMethod = .get
        let path: String = "approveReview.json"
        
        let id_comment: Int
        var parameters: Parameters? {
            return [
                "id_comment": id_comment,
            ]
        }
    }
}
