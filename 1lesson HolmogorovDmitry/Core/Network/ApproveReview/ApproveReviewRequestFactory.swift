//
//  ApproveReviewRequestFactory.swift
//  1lesson HolmogorovDmitry
//
//  Created by Дмитрий on 06/05/2019.
//  Copyright © 2019 Dmitry. All rights reserved.
//

import Foundation
import Alamofire

protocol ApproveReviewRequestFactory {
    func approveReview(id_comment: Int,
                   completionHandler: @escaping (DataResponse<ApproveReviewResult>) -> Void)
}
