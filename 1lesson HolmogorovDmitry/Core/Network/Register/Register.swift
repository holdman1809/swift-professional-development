import Foundation
import Alamofire

class Register: AbstractRequestFactory {
    let errorParser: AbstractErrorParser
    let sessionManager: SessionManager
    let queue: DispatchQueue?
    let baseUrl = URL(string: "https://raw.githubusercontent.com/GeekBrainsTutorial/online-store-api/master/responses/")!
    
    init(errorParser: AbstractErrorParser,
         sessionManager: SessionManager,
         queue: DispatchQueue? = DispatchQueue.global(qos: .utility)) {
        
        self.errorParser = errorParser
        self.sessionManager = sessionManager
        self.queue = queue
    }
}

extension Register: RegisterRequestFactory {
    func register(id_user: Int, userName: String, password: String, email: String, gender: String, credit_card: String, bio: String, completionHandler: @escaping (DataResponse<RegisterResult>) -> Void) {
        let regModel = Register(baseUrl: baseUrl, id_user: id_user, username: userName, password: password, email: email, gender: gender, credit_card: gender, bio: bio)
        self.request(request: regModel as URLRequestConvertible, completionHandler: completionHandler).session.finishTasksAndInvalidate()
    }

}

extension Register {
    struct Register: RequestRouter {
        let baseUrl: URL
        let method: HTTPMethod = .get
        let path: String = "registerUser.json"
        
        let id_user: Int
        let username: String
        let password: String
        let email: String
        let gender: String
        let credit_card: String
        let bio: String
        var parameters: Parameters? {
            return [
                "id_user": id_user,
                "username": username,
                "password": password,
                "email": email,
                "gender": gender,
                "credit_card": credit_card,
                "bio": bio
            ]
        }
    }
}
