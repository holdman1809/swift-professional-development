//
//  CatalogDataRequestFactory.swift
//  1lesson HolmogorovDmitry
//
//  Created by Дмитрий on 25/04/2019.
//  Copyright © 2019 Dmitry. All rights reserved.
//

import Foundation
import Alamofire

protocol CatalogDataRequestFactory {
    func catalog(page_number: Int, id_category: Int, completionHandler: @escaping (DataResponse<CatalogDataResult>) -> Void)
}
