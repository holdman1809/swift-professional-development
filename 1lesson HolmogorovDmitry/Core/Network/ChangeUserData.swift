import Foundation
import Alamofire

class ChangeData: AbstractRequestFactory {
    let errorParser: AbstractErrorParser
    let sessionManager: SessionManager
    let queue: DispatchQueue?
    let baseUrl = URL(string: "https://raw.githubusercontent.com/GeekBrainsTutorial/online-store-api/master/responses/")!
    
    init(errorParser: AbstractErrorParser,
         sessionManager: SessionManager,
         queue: DispatchQueue? = DispatchQueue.global(qos: .utility)) {
        
        self.errorParser = errorParser
        self.sessionManager = sessionManager
        self.queue = queue
    }
}

extension ChangeData: ChangeUserDataRequestFactory {
    func changeUserData(id_user: Int, userName: String, password: String, email: String, gender: String, credit_card: String, bio: String, completionHandler: @escaping (DataResponse<ChangeUserDataResult>) -> Void) {
        let changeData = ChangeData(baseUrl: baseUrl, id_user: id_user, username: userName, password: password, email: email, gender: gender, credit_card: gender, bio: bio)
        self.request(request: changeData as URLRequestConvertible, completionHandler: completionHandler).session.finishTasksAndInvalidate()
    }
}

extension ChangeData {
    struct ChangeData: RequestRouter {
        let baseUrl: URL
        let method: HTTPMethod = .get
        let path: String = "changeUserData.json"
        
        let id_user: Int
        let username: String
        let password: String
        let email: String
        let gender: String
        let credit_card: String
        let bio: String
        var parameters: Parameters? {
            return [
                "id_user": id_user,
                "username": username,
                "password": password,
                "email": email,
                "gender": gender,
                "credit_card": credit_card,
                "bio": bio
            ]
        }
    }
}
