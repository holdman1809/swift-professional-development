//
//  ChangeUserDataRequestFactory.swift
//  1lesson HolmogorovDmitry
//
//  Created by Дмитрий on 22/04/2019.
//  Copyright © 2019 Dmitry. All rights reserved.
//

import Foundation
import Alamofire

protocol ChangeUserDataRequestFactory {
    func changeUserData(id_user: Int, userName: String, password: String, email: String, gender: String, credit_card: String, bio: String, completionHandler: @escaping (DataResponse<ChangeUserDataResult>) -> Void)
}
