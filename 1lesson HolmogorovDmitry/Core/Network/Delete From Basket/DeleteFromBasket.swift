//
//  DeleteFromBasket.swift
//  1lesson HolmogorovDmitry
//
//  Created by Дмитрий on 25/04/2019.
//  Copyright © 2019 Dmitry. All rights reserved.
//

import Foundation
import Alamofire

class DeleteFromBasket: AbstractRequestFactory {
    let errorParser: AbstractErrorParser
    let sessionManager: SessionManager
    let queue: DispatchQueue?
    let baseUrl = URL(string: "https://raw.githubusercontent.com/GeekBrainsTutorial/online-store-api/master/responses/")!
    
    init(errorParser: AbstractErrorParser,
         sessionManager: SessionManager,
         queue: DispatchQueue? = DispatchQueue.global(qos: .utility)) {
        
        self.errorParser = errorParser
        self.sessionManager = sessionManager
        self.queue = queue
    }
}

extension DeleteFromBasket: DeleteFromBasketRequestFactory {
    func deleteFromBasket(id_product: Int, quantity: Int, completionHandler: @escaping (DataResponse<DeleteFromBasketResult>) -> Void) {
        let requestModel = Delete_FromBasket(baseUrl: baseUrl, id_product: id_product)
        self.request(request: requestModel, completionHandler: completionHandler).session.finishTasksAndInvalidate()
        }
    }

extension DeleteFromBasket {
    struct Delete_FromBasket: RequestRouter {
        let baseUrl: URL
        let method: HTTPMethod = .get
        let path: String = "deleteFromBasket.json"
        
        let id_product: Int
        var parameters: Parameters? {
            return [
                "id_product": id_product
            ]
        }
    }
}
