//
//  AddReview.swift
//  1lesson HolmogorovDmitry
//
//  Created by Дмитрий on 06/05/2019.
//  Copyright © 2019 Dmitry. All rights reserved.
//

import Foundation
import Alamofire

class AddReview: AbstractRequestFactory {
    let errorParser: AbstractErrorParser
    let sessionManager: SessionManager
    let queue: DispatchQueue?
    let baseUrl = URL(string: "https://raw.githubusercontent.com/GeekBrainsTutorial/online-store-api/master/responses/")!
    
    init(errorParser: AbstractErrorParser,
         sessionManager: SessionManager,
         queue: DispatchQueue? = DispatchQueue.global(qos: .utility)) {
        
        self.errorParser = errorParser
        self.sessionManager = sessionManager
        self.queue = queue
    }
}

extension AddReview: AddReviewRequestFactory {
    func addReview(id_user: Int, text: String, completionHandler: @escaping (DataResponse<AddReviewResult>) -> Void) {
        let requestModel = Review(baseUrl: baseUrl, id_user: id_user, text: text)
        self.request(request: requestModel, completionHandler: completionHandler).session.finishTasksAndInvalidate()
    }
}

extension AddReview {
    struct Review: RequestRouter {
        let baseUrl: URL
        let method: HTTPMethod = .get
        let path: String = "addReview.json"
        
        let id_user: Int
        let text: String
        var parameters: Parameters? {
            return [
                "id_user": id_user,
                "text": text
            ]
        }
    }
}
