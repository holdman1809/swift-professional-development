//
//  RemoveReviewRequestFactory.swift
//  1lesson HolmogorovDmitry
//
//  Created by Дмитрий on 06/05/2019.
//  Copyright © 2019 Dmitry. All rights reserved.
//

import Foundation
import Alamofire

protocol RemoveReviewRequestFactory {
    func removeReview(id_comment: Int,
                   completionHandler: @escaping (DataResponse<RemoveReviewResult>) -> Void)
}
