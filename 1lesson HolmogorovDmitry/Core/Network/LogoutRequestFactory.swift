//
//  LogoutRequestFactory.swift
//  1lesson HolmogorovDmitry
//
//  Created by Дмитрий on 22/04/2019.
//  Copyright © 2019 Dmitry. All rights reserved.
//

import Foundation
import Alamofire

protocol LogoutRequestFactory {
    func logout(id_user: Int, completionHandler: @escaping (DataResponse<LogoutResult>) -> Void)
}
