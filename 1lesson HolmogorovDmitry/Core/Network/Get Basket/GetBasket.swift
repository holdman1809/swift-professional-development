//
//  GetBasket.swift
//  1lesson HolmogorovDmitry
//
//  Created by Дмитрий on 25/04/2019.
//  Copyright © 2019 Dmitry. All rights reserved.
//

import Foundation
import Alamofire

class GetBasket: AbstractRequestFactory {
    let errorParser: AbstractErrorParser
    let sessionManager: SessionManager
    let queue: DispatchQueue?
    let baseUrl = URL(string: "http://localhost:8080/")!
    
    init(errorParser: AbstractErrorParser,
         sessionManager: SessionManager,
         queue: DispatchQueue? = DispatchQueue.global(qos: .utility)) {
        
        self.errorParser = errorParser
        self.sessionManager = sessionManager
        self.queue = queue
    }
}

extension GetBasket: GetBasketRequestFactory {
    func getBasket(amount: String, countGoods: String, id_product: String, product_name: String, price: String, quantity: String, completionHandler: @escaping (DataResponse<GetBasketResult>) -> Void) {
        let requestModel = Get_Basket(baseUrl: baseUrl, amount: amount, countGoods: countGoods, id_product: id_product, product_name: product_name, price: price, quantity: quantity)
        
        self.request(request: requestModel, completionHandler: completionHandler).session.finishTasksAndInvalidate()
    }
}
extension GetBasket {
    struct Get_Basket: RequestRouter {
        let baseUrl: URL
        let method: HTTPMethod = .post
        let path: String = "getbasket"
        
        let amount: String
        let countGoods: String
        let id_product: String
        let product_name: String
        let price: String
        let quantity: String
        
        var parameters: Parameters? {
            return [
                "amount": amount,
                "countGoods": countGoods,
                "id_product": id_product,
                "product_name": product_name,
                "price": price,
                "quantity": quantity,
            ]
        }
    }
}
