//
//  GetBasketRequestFactory.swift
//  1lesson HolmogorovDmitry
//
//  Created by Дмитрий on 25/04/2019.
//  Copyright © 2019 Dmitry. All rights reserved.
//

import Foundation
import Alamofire

protocol GetBasketRequestFactory {
    func getBasket(amount: String, countGoods: String, id_product: String, product_name: String, price: String, quantity: String, completionHandler: @escaping (DataResponse<GetBasketResult>) -> Void)
}
