//
//  ErrorParser.swift
//  1lesson HolmogorovDmitry
//
//  Created by Дмитрий on 21/04/2019.
//  Copyright © 2019 Dmitry. All rights reserved.
//

import Foundation

class ErrorParser: AbstractErrorParser {
    func parse(_ result: Error) -> Error {
        return result
    }
    
    func parse(response: HTTPURLResponse?, data: Data?, error: Error?) -> Error? {
        return error
    }
}
