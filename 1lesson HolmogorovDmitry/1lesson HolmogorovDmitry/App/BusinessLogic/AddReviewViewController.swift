//
//  AddReviewViewController.swift
//  1lesson HolmogorovDmitry
//
//  Created by Дмитрий on 22/05/2019.
//  Copyright © 2019 Dmitry. All rights reserved.
//

import UIKit

///Форма создания своего отзыва для конкретного товара
class AddReviewViewController: UIViewController {
    
    var identificatorReview: Int?
    private var userIdLabel: UILabel?
    private var userId: UILabel?
    private var review: UITextView?
    private var confirmButton: UIButton?
    var reviewDict = [Int?:[Review]]()
 
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.view.backgroundColor = UIColor.white
        self.createUI()
    }
    
    private func createUI(){
        let leftMargin: CGFloat = 20.0
        let topMargin: CGFloat = 150.0
        let uiInterspace: CGFloat = 7
        
        self.userIdLabel = UILabel(frame: CGRect(x: leftMargin, y: topMargin, width: 100, height: 20))
        self.userIdLabel?.text = "User ID"
        self.userIdLabel?.textAlignment = .right
        self.userIdLabel?.textColor = UIColor.black
        self.view.addSubview(self.userIdLabel!)
        
        self.userId = UILabel(frame: CGRect(x: leftMargin + (self.userIdLabel?.bounds.width)!, y: topMargin, width: 60, height: 20))
        self.userId?.text = String(UserDefaults.standard.integer(forKey: "id"))
        self.userId?.textAlignment = .left
        self.userId?.textColor = UIColor.black
        self.view.addSubview(self.userId!)
        
        self.review = UITextView(frame: CGRect(x: leftMargin, y: (self.userIdLabel?.frame.maxY)! + uiInterspace, width: self.view.bounds.width - leftMargin * 2, height: self.view.bounds.height - (self.userIdLabel?.frame.maxY)! - topMargin - uiInterspace))
        self.review?.clipsToBounds = true
        self.review?.layer.cornerRadius = 10
        self.review?.layer.borderColor = UIColor.blue.cgColor
        self.review?.layer.borderWidth = 0.3
        self.review?.textAlignment = .left
        self.review?.text = NSLocalizedString("Review:", comment: "")
        self.review?.font = UIFont.systemFont(ofSize: 20)
        self.view.addSubview(self.review!)
        
        self.confirmButton = UIButton(frame: CGRect(x: self.view.bounds.width / 2 - 75, y: (self.review?.frame.maxY)! + uiInterspace, width: 150, height: 40))
        self.confirmButton?.setTitle(NSLocalizedString("Confirm", comment: ""), for: .normal)
        self.confirmButton?.layer.borderWidth = 0.3
        self.confirmButton?.layer.borderColor = UIColor.blue.cgColor
        self.confirmButton?.setTitleColor(UIColor.black, for: .normal)
        self.confirmButton?.setTitleColor(UIColor.red, for: .selected)
        self.confirmButton?.clipsToBounds = true
        self.confirmButton?.layer.cornerRadius = 10
        self.confirmButton?.addTarget(self, action: #selector(confirmBtnTapped), for: .touchUpInside)
        self.view.addSubview(self.confirmButton!)
    }
    
    @objc
    func confirmBtnTapped() {
        let addReview = RequestFactory().makeAddReviewRequestFactory()
        addReview.addReview(id_user: UserDefaults.standard.integer(forKey: "id"), text: self.review!.text, completionHandler: { (response) in
            switch response.result {
            case .success(let reviewResult):
                print(reviewResult)
                DispatchQueue.main.async {
                    //заполняем синглтон данными чтобы использовать их в другом контроллере
                    Review.shared.id =  self.userId!.text!
                    Review.shared.review = self.review!.text
                    self.reviewDict = [self.identificatorReview : [Review.shared]]
                    
                    let reviewVC = ReviewViewController()
                    reviewVC.indexForReview = self.identificatorReview
                    reviewVC.reviewDict[self.identificatorReview] = [Review.shared]
                    
                    self.navigationController?.pushViewController(reviewVC, animated: true)
                }
            case .failure(let error):
                print(error.localizedDescription)
            }
        })
     }
}
