//
//  HandlerInfoViewController.swift
//  1lesson HolmogorovDmitry
//
//  Created by Дмитрий on 15/05/2019.
//  Copyright © 2019 Dmitry. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper

//форма для авторизации
class MainViewController: UIViewController {
    
    private var login: UITextField?
    private var password: UITextField?
    private var signIn: UIButton?
    private var register: UILabel?

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.view.backgroundColor = UIColor.white
        self.createUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(registerTapped))
        self.register?.addGestureRecognizer(recognizer)
        
    }
    
    private func createUI(){
        self.login = UITextField(frame: CGRect(x: self.view.bounds.width / 2 - 125, y: self.view.bounds.height / 3 - 20, width: 250, height: 40))
        self.login?.clipsToBounds = true
        self.login?.layer.cornerRadius = 10
        self.login?.placeholder = NSLocalizedString("Login", comment: "")
        self.login?.layer.borderColor = UIColor.blue.cgColor
        self.login?.layer.borderWidth = 0.3
        self.login?.textAlignment = .center
        self.login?.accessibilityIdentifier = "Логин"
        self.view.addSubview(self.login!)
        
        self.password = UITextField(frame: CGRect(x: self.view.bounds.width / 2 - 125, y: self.view.bounds.height / 3 + 23, width: 250, height: 40))
        self.password?.placeholder = NSLocalizedString("Password", comment: "")
        self.password?.clipsToBounds = true
        self.password?.layer.cornerRadius = 10
        self.password?.layer.borderColor = UIColor.blue.cgColor
        self.password?.layer.borderWidth = 0.3
        self.password?.textAlignment = .center
        self.password?.accessibilityIdentifier = "Пароль"
        self.view.addSubview(self.password!)
        
        self.signIn = UIButton(frame: CGRect(x: self.view.bounds.width / 2 + 25, y: self.view.bounds.height / 3 + 70, width: 100, height: 20))
        self.signIn?.setTitle(NSLocalizedString("Sign in", comment: ""), for: .normal)
        self.signIn?.layer.borderWidth = 0.3
        self.signIn?.layer.borderColor = UIColor.blue.cgColor
        self.signIn?.setTitleColor(UIColor.black, for: .normal)
        self.signIn?.setTitleColor(UIColor.red, for: .selected)
        self.signIn?.clipsToBounds = true
        self.signIn?.layer.cornerRadius = 10
        self.signIn?.addTarget(self, action: #selector(signInTapped), for: .touchUpInside)
        self.signIn?.accessibilityIdentifier = "Войти"
        self.view.addSubview(self.signIn!)
        
        self.register = UILabel(frame: CGRect(x: (self.password?.frame.origin.x)!, y: self.view.bounds.height / 3 + 70, width: 140, height: 20))
        self.register?.text = NSLocalizedString("Registration", comment: "")
        self.register?.textAlignment = .left
        self.register?.textColor = UIColor.blue
        self.register?.isUserInteractionEnabled = true
        self.view.addSubview(self.register!)
    }
    
    @objc
    func signInTapped(){
        
        if self.login?.text == KeychainWrapper.standard.string(forKey: "login") &&  self.password?.text == KeychainWrapper.standard.string(forKey: "password"){
            
            let nextVC = SettingsViewController()
            self.navigationController?.pushViewController(nextVC, animated: true)
            
        } else {
            self.alertController()
        }
    }
    private func alertController() {
        let alert = UIAlertController(title: "Упс", message: "Не корректный логин или пароль", preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: .cancel) { (action) in
            
        }
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    @objc
    func registerTapped(){
        let regVC = RegistrationViewController()
        self.navigationController?.pushViewController(regVC, animated: true)
    }
    
    
}
