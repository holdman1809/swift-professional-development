//
//  ReviewModel.swift
//  1lesson HolmogorovDmitry
//
//  Created by Дмитрий on 23/05/2019.
//  Copyright © 2019 Dmitry. All rights reserved.
//

import Foundation

///синглтон для доступа к отзывам из любой точки приложения(костыли для дз)
class Review {
    static let shared = Review()

    var id: String?
    var review: String?

    private init (){}
}


