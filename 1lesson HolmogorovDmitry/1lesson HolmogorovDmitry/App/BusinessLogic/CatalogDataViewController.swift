//
//  CatalogDataViewController.swift
//  1lesson HolmogorovDmitry
//
//  Created by Дмитрий on 20/05/2019.
//  Copyright © 2019 Dmitry. All rights reserved.
//

import UIKit


///Экран показа списка товаров
class CatalogDataViewController: UITableViewController {
    private var catalogArray = [Products]()
    let basketViewController = BasketViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "catalogCell")
        let rightBarButton = UIBarButtonItem(image: UIImage(named: "basket"), style: .plain, target: self, action: #selector(basketBarBtnTaped))
        self.navigationItem.rightBarButtonItem = rightBarButton

        let catalogData = RequestFactory().makeCatalogDataRequestFatory()
        catalogData.catalog(page_number: 1, id_category: 1) { (response) in
            switch response.result {
            case .success(let catalogData):
                DispatchQueue.main.async {
                    self.catalogArray = catalogData
                    self.tableView.reloadData()
                }
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.view.backgroundColor = UIColor.white
    }
    
    @objc
    func basketBarBtnTaped() {
        self.navigationController?.pushViewController(self.basketViewController, animated: true)
    }
}

//MARK: - UITableViewDataSource
extension CatalogDataViewController{
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.catalogArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //так как данных не так много то решил делать без реюза ячеек
        let cell = UITableViewCell(style: UITableViewCell.CellStyle.subtitle, reuseIdentifier: "catalogCell")
        
        cell.textLabel?.text = self.catalogArray[indexPath.row].product_name
        cell.detailTextLabel?.text = String(self.catalogArray[indexPath.row].price) + "₽"
        
        return cell
    }
}

//MARK: - UITableViewDelegate
extension CatalogDataViewController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.alertController(title: "", message: "Что необходимо сделать?", complitionForAddReview: {
            let addReviewVC = AddReviewViewController()
            addReviewVC.identificatorReview = indexPath.row
            self.navigationController?.pushViewController(addReviewVC, animated: true)
        }, complitionForShowReview: {
            
            let reviewViewController = ReviewViewController()
            reviewViewController.indexForReview = indexPath.row
            reviewViewController.reviewDict[indexPath.row] = [Review.shared] //из-за синглтона отзывы для товаров у нас будут одинаковые) с сервера надо тянуть отзывы или кордату подключать)
            
            self.navigationController?.pushViewController(reviewViewController, animated: true)
        }) {
            self.basketViewController.basketArray.append(self.catalogArray[indexPath.row])
            self.alertAddToBasket(title: "", message: NSLocalizedString("Item successfully added to cart", comment: ""))
        }
    }
}

//MARK: - private methods
extension CatalogDataViewController{
    private func alertController(title: String, message: String, complitionForAddReview: @escaping () -> (), complitionForShowReview: @escaping () -> (), complitionAddToBasket: @escaping () -> ()){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        let actionAddReview = UIAlertAction(title: "Добавить отзыв", style: .default) { (action) in
            complitionForAddReview()
        }
        let actionShowReview = UIAlertAction(title: "Показать отзывы", style: .default) { (action) in
            complitionForShowReview()
        }
        let actionAddToBasket = UIAlertAction(title: "Добавить в корзину", style: .default) { (action) in
            complitionAddToBasket()
        }
        alert.addAction(actionAddReview)
        alert.addAction(actionShowReview)
        alert.addAction(actionAddToBasket)
        self.present(alert, animated: true, completion: nil)
    }
    private func alertAddToBasket(title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
}
