//
//  ReviewViewController.swift
//  1lesson HolmogorovDmitry
//
//  Created by Дмитрий on 22/05/2019.
//  Copyright © 2019 Dmitry. All rights reserved.
//

import UIKit

///форма показа отзывов
class ReviewViewController: UITableViewController {
    
    var indexForReview: Int?
    var reviewDict = [Int?: [Review]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.dataSource = self
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "reviewCell")
        self.tableView.rowHeight = 150
    }
}

extension ReviewViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.reviewDict[self.indexForReview]?.count ?? 1
//        if self.reviewDict[self.indexForReview]!.count > 0 {
//            return self.reviewDict[self.indexForReview]!.count
//        } else {
//            return 1
//        }
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "reviewCell", for: indexPath)

        cell.textLabel!.text = self.reviewDict[self.indexForReview]?[indexPath.row].review ?? "!!! Отзывов пока нет !!!"
        print(self.reviewDict[indexPath.row]?.first?.review as Any)
       
        return cell
    }
  
}
