//
//  RegistrationViewController.swift
//  1lesson HolmogorovDmitry
//
//  Created by Дмитрий on 15/05/2019.
//  Copyright © 2019 Dmitry. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper

/// форма для регистрации нового пользователя
class RegistrationViewController: UIViewController {

    private var idUser: UILabel?
    private var idUserLabel: UILabel?
    private var userName: UITextField?
    private var password: UITextField?
    private var email: UITextField?
    private var gender: UITextField?
    private var creditCard: UITextField?
    private var bio: UITextView?
    private var confirmButton: UIButton?
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.view.backgroundColor = UIColor.white
        self.createUI()
    }

    
    private func createUI(){
        let leftMargin: CGFloat = 20.0
        let topMargin: CGFloat = 150.0
        let uiInterspace: CGFloat = 7
        
        self.idUserLabel = UILabel(frame: CGRect(x: leftMargin, y: topMargin, width: 100, height: 20))
        self.idUserLabel?.text = "User ID"
        self.idUserLabel?.textAlignment = .right
        self.idUserLabel?.textColor = UIColor.black
        self.view.addSubview(self.idUserLabel!)
        
        self.idUser = UILabel(frame: CGRect(x: leftMargin + (self.idUserLabel?.bounds.width)!, y: topMargin, width: 60, height: 20))
        self.idUser?.text = String(arc4random_uniform(151))
        self.idUser?.textAlignment = .left
        self.idUser?.textColor = UIColor.black
        self.view.addSubview(self.idUser!)
        
        self.userName = UITextField(frame: CGRect(x: leftMargin, y: topMargin + (self.idUserLabel?.bounds.height)! + uiInterspace, width: 200, height: 40))
        self.userName?.placeholder = NSLocalizedString("Login", comment: "")
        self.userName?.clipsToBounds = true
        self.userName?.layer.cornerRadius = 10
        self.userName?.layer.borderColor = UIColor.blue.cgColor
        self.userName?.layer.borderWidth = 0.3
        self.userName?.textAlignment = .center
        self.view.addSubview(self.userName!)
        
        self.password = UITextField(frame: CGRect(x: leftMargin, y: (self.userName?.frame.maxY)! + uiInterspace, width: 200, height: 40))
        self.password?.placeholder = NSLocalizedString("Password", comment: "")
        self.password?.clipsToBounds = true
        self.password?.layer.cornerRadius = 10
        self.password?.layer.borderColor = UIColor.blue.cgColor
        self.password?.layer.borderWidth = 0.3
        self.password?.textAlignment = .center
        self.view.addSubview(self.password!)
        
        self.email = UITextField(frame: CGRect(x: leftMargin, y: (self.password?.frame.maxY)! + uiInterspace, width: 200, height: 40))
        self.email?.placeholder = NSLocalizedString("Email", comment: "")
        self.email?.clipsToBounds = true
        self.email?.layer.cornerRadius = 10
        self.email?.layer.borderColor = UIColor.blue.cgColor
        self.email?.layer.borderWidth = 0.3
        self.email?.textAlignment = .center
        self.view.addSubview(self.email!)
        
        self.gender = UITextField(frame: CGRect(x: leftMargin, y: (self.email?.frame.maxY)! + uiInterspace, width: 40, height: 40))
        self.gender?.placeholder = NSLocalizedString("Gender", comment: "")
        self.gender?.clipsToBounds = true
        self.gender?.layer.cornerRadius = 10
        self.gender?.layer.borderColor = UIColor.blue.cgColor
        self.gender?.layer.borderWidth = 0.3
        self.gender?.textAlignment = .center
        self.view.addSubview(self.gender!)
        
        self.creditCard = UITextField(frame: CGRect(x: leftMargin, y: (self.gender?.frame.maxY)! + uiInterspace, width: 200, height: 40))
        self.creditCard?.placeholder = NSLocalizedString("Payment Card", comment: "")
        self.creditCard?.clipsToBounds = true
        self.creditCard?.layer.cornerRadius = 10
        self.creditCard?.layer.borderColor = UIColor.blue.cgColor
        self.creditCard?.layer.borderWidth = 0.3
        self.creditCard?.textAlignment = .center
        self.view.addSubview(self.creditCard!)
        
        self.bio = UITextView(frame: CGRect(x: leftMargin, y: (self.creditCard?.frame.maxY)! + uiInterspace, width: self.view.bounds.width - leftMargin * 2, height: self.view.bounds.height - (self.creditCard?.frame.maxY)! - topMargin - uiInterspace))
        self.bio?.clipsToBounds = true
        self.bio?.layer.cornerRadius = 10
        self.bio?.layer.borderColor = UIColor.blue.cgColor
        self.bio?.layer.borderWidth = 0.3
        self.bio?.textAlignment = .left
        self.bio?.text = NSLocalizedString("Bio:", comment: "")
        self.bio?.font = UIFont.systemFont(ofSize: 20)
        self.view.addSubview(self.bio!)
        
        self.confirmButton = UIButton(frame: CGRect(x: self.view.bounds.width / 2 - 75, y: (self.bio?.frame.maxY)! + uiInterspace, width: 150, height: 40))
        self.confirmButton?.setTitle(NSLocalizedString("Confirm", comment: ""), for: .normal)
        self.confirmButton?.layer.borderWidth = 0.3
        self.confirmButton?.layer.borderColor = UIColor.blue.cgColor
        self.confirmButton?.setTitleColor(UIColor.black, for: .normal)
        self.confirmButton?.setTitleColor(UIColor.red, for: .selected)
        self.confirmButton?.clipsToBounds = true
        self.confirmButton?.layer.cornerRadius = 10
        self.confirmButton?.addTarget(self, action: #selector(confirmBtnTapped), for: .touchUpInside)
        self.view.addSubview(self.confirmButton!)
    }
    
    @objc
    func confirmBtnTapped(){

        KeychainWrapper.standard.set(self.userName!.text!, forKey: "login")
        KeychainWrapper.standard.set(self.password!.text!, forKey: "password")
        UserDefaults.standard.set(Int(self.idUser?.text ?? "01"), forKey: "id")
        UserDefaults.standard.set(self.email?.text, forKey: "email")
        UserDefaults.standard.set(self.gender?.text, forKey: "gender")
        UserDefaults.standard.set(self.creditCard?.text, forKey: "card")
        UserDefaults.standard.set(self.bio?.text, forKey: "bio")
        
        self.registerRequest()
    }
    
    private func registerRequest() {
        
        guard let login = KeychainWrapper.standard.string(forKey: "login") else { fatalError()}
        guard let password = KeychainWrapper.standard.string(forKey: "password") else { fatalError()}
        guard let email = UserDefaults.standard.string(forKey: "email") else { fatalError()}
        guard let gender = UserDefaults.standard.string(forKey: "gender") else { fatalError()}
        guard let card = UserDefaults.standard.string(forKey: "card") else { fatalError()}
        guard let bio = UserDefaults.standard.string(forKey: "bio") else { fatalError()}
        
        let register = RequestFactory().makeRegisterRequestFatory()
        
        register.register(id_user: UserDefaults.standard.integer(forKey: "id"), userName: login, password: password, email: email, gender: gender, credit_card: card, bio: bio, completionHandler: { (response) in
            
            switch response.result {
            case .success(let register):
                DispatchQueue.main.async {
                    self.alertСurrying(complition: {
                        let settingsVC = SettingsViewController()
                        self.navigationController?.pushViewController(settingsVC, animated: true)
                    })(NSLocalizedString("Success", comment: ""))(register.userMessage)
                }
            case .failure(let error):
                print(error.localizedDescription)
            }
        })
    }
    
    private func alertСurrying(complition: @escaping () -> ()) -> (String) -> ((String) -> ()){
        return { title in
            return  { message in
                let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
                let action = UIAlertAction(title: "Ok", style: .default) { (action) in
                    complition()
                }
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
}

