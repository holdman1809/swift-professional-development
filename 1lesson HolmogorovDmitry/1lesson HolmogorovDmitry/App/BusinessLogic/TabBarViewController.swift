//
//  TabBarViewController.swift
//  1lesson HolmogorovDmitry
//
//  Created by Дмитрий on 17/04/2019.
//  Copyright © 2019 Dmitry. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController {

    override func viewDidLoad() {
        self.viewControllers = self.createArray()
        self.tabBar.tintColor = UIColor.black
    }
    func createArray() -> (Array<UIViewController>){
        var controllersArray = [UIViewController]()
        
        let vc = MainViewController.init()
        vc.title = NSLocalizedString("Main", comment: "")
        vc.tabBarItem = UITabBarItem.init(title: NSLocalizedString("Main", comment: ""), image: UIImage(named: "home"), selectedImage: UIImage(named: "home"))
        let navVC = UINavigationController.init(rootViewController: vc)
        navVC.navigationBar.prefersLargeTitles = true
        controllersArray.append(navVC)
        
        let catalogVC = CatalogDataViewController.init()
        catalogVC.title = NSLocalizedString("Catalog", comment: "")
        catalogVC.tabBarItem = UITabBarItem.init(title: NSLocalizedString("Catalog", comment: ""), image: UIImage(named: "catalog"), selectedImage: UIImage(named: "catalog"))
        let navCatalogVC = UINavigationController.init(rootViewController: catalogVC)
        navCatalogVC.navigationBar.prefersLargeTitles = true
        controllersArray.append(navCatalogVC)
        
        return controllersArray
    }

}
