//
//  UserProfil.swift
//  1lesson HolmogorovDmitry
//
//  Created by Дмитрий on 15/05/2019.
//  Copyright © 2019 Dmitry. All rights reserved.
//

import Foundation

class UserProfil {
    
    static let shared = UserProfil()
    
    var idUser: Int?
    var userName: String?
    var password: String?
    var email: String?
    var gender: String?
    var creditCard: String?
    var bio: String?
    
    private init() { }
    
}
