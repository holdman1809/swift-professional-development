//
//  BasketViewController.swift
//  1lesson HolmogorovDmitry
//
//  Created by Дмитрий on 27/05/2019.
//  Copyright © 2019 Dmitry. All rights reserved.
//

import UIKit

class BasketViewController: UITableViewController {
    var basketArray = [Products]()
    var totalPriceButton: UIBarButtonItem?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.tableView.dataSource = self
        self.totalPriceButton = UIBarButtonItem(title: self.price(), style: .done, target: self, action: #selector(totalPriceBtnTaped))
        self.navigationItem.rightBarButtonItem = totalPriceButton
    }
    
    @objc
    func totalPriceBtnTaped(){
        self.paymentAlert(title: NSLocalizedString("Success", comment: ""), message: NSLocalizedString("Item successfully paid", comment: "")) { }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.view.backgroundColor = UIColor.white
        self.totalPriceButton?.title = self.price()
        self.tableView.reloadData()
    }
}

//MARK: - UITableViewDataSource
extension BasketViewController{
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.basketArray.count > 0 {
            return self.basketArray.count
        } else {
            self.paymentAlert(title: "Внимание", message: "В корзине пока пусто") {
                self.navigationController?.popViewController(animated: true)
            }
            return 0
        }
       
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = self.basketArray[indexPath.row].product_name + " " + String(self.basketArray[indexPath.row].price) + "₽"
        return cell
    }
}

//MARK: - private func
extension BasketViewController{
    private func paymentAlert(title: String, message: String, complitionPayment: @escaping () -> ()){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: .cancel) { (action) in
            complitionPayment()
        }
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    
    ///метод пересчитывает сумму товаров
    private func price() -> String {
        var totalPrice = 0
        for i in self.basketArray{
            totalPrice += i.price
        }
        return NSLocalizedString("Total price ", comment: "") + String(totalPrice) + "₽"
    }
}
