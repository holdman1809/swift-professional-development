//
//  SettingsViewController.swift
//  1lesson HolmogorovDmitry
//
//  Created by Дмитрий on 17/04/2019.
//  Copyright © 2019 Dmitry. All rights reserved.
//

import UIKit

///коонтроллер для регистрации нового юзера и просмотра и изменения информации
class SettingsViewController: UITableViewController {
    private var settingsArray = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.tableView.rowHeight = 100
        
        self.settingsArray = [NSLocalizedString("Register", comment: ""), NSLocalizedString("User info", comment: "")]
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.view.backgroundColor = UIColor.white
    }
}

extension SettingsViewController {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.settingsArray.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cellIdentifier = "cell"
        let cell: UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
        
        cell?.accessoryType = UITableViewCell.AccessoryType.disclosureIndicator
        cell?.textLabel?.text = self.settingsArray[indexPath.row]
        return cell!
    }
}

extension SettingsViewController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 { 
            let register = RegistrationViewController()
            self.navigationController?.pushViewController(register, animated: true)
        }
        if indexPath.row == 1 {
            let userInfo = UserInfoViewController()
            self.navigationController?.pushViewController(userInfo, animated: true)
        }
    }
}
