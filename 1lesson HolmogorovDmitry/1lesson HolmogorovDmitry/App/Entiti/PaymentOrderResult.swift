//
//  PaymentOrderResult.swift
//  1lesson HolmogorovDmitry
//
//  Created by Дмитрий on 10/05/2019.
//  Copyright © 2019 Dmitry. All rights reserved.
//

import Foundation

struct PaymentOrderResult: Codable {
    let order: String
    let totalPrice: String
    let basket: Basket
}

struct Basket: Codable {
    let id_product: String
    let product_name: String
    let price: String
    let quantity: String
    
    enum CodingKeys: String, CodingKey {
        case id_product = "id_product"
        case product_name = "product_name"
        case price = "price"
        case quantity = "quantity"
    }
}
