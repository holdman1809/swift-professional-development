//
//  RegisterResult.swift
//  1lesson HolmogorovDmitry
//
//  Created by Дмитрий on 22/04/2019.
//  Copyright © 2019 Dmitry. All rights reserved.
//

import Foundation

struct RegisterResult: Codable {
    let result: Int
    let userMessage: String

}
