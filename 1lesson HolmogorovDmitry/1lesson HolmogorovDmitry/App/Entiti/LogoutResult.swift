//
//  LogoutResult.swift
//  1lesson HolmogorovDmitry
//
//  Created by Дмитрий on 22/04/2019.
//  Copyright © 2019 Dmitry. All rights reserved.
//

import Foundation

struct LogoutResult: Codable {
    let result: Int
    
    enum CodingKeys: String, CodingKey {
        case result = "result"
    }
}
