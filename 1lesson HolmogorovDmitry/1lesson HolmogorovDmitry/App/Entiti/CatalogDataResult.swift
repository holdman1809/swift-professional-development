//
//  CatalogDataResult.swift
//  1lesson HolmogorovDmitry
//
//  Created by Дмитрий on 25/04/2019.
//  Copyright © 2019 Dmitry. All rights reserved.
//

import Foundation

typealias CatalogDataResult = [Products]

struct Products: Codable {
    let id_product: Int
    let product_name: String
    let price: Int
    
    enum CodingKeys: String, CodingKey {
        case id_product = "id_product"
        case product_name = "product_name"
        case price = "price"
    }
}



