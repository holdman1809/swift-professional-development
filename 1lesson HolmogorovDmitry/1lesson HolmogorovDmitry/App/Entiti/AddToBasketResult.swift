//
//  AddToBasket.swift
//  1lesson HolmogorovDmitry
//
//  Created by Дмитрий on 25/04/2019.
//  Copyright © 2019 Dmitry. All rights reserved.
//

import Foundation

struct AddToBasketResult: Codable {
    let result: Int
}
