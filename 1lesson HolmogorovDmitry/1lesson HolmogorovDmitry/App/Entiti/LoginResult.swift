//
//  LoginResult.swift
//  1lesson HolmogorovDmitry
//
//  Created by Дмитрий on 21/04/2019.
//  Copyright © 2019 Dmitry. All rights reserved.
//

import Foundation

struct LoginResult: Codable {
    let result: Int
    let user: User
    
    
}
