//
//  AddReviewResult.swift
//  1lesson HolmogorovDmitry
//
//  Created by Дмитрий on 06/05/2019.
//  Copyright © 2019 Dmitry. All rights reserved.
//

import Foundation

struct AddReviewResult: Codable {
    let result: Int
    let userMessage: String
}
