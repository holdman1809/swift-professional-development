//
//  GetBasketResult.swift
//  1lesson HolmogorovDmitry
//
//  Created by Дмитрий on 25/04/2019.
//  Copyright © 2019 Dmitry. All rights reserved.
//

import Foundation

struct GetBasketResult: Codable {
    let amount: String
    let countGoods: String
    let contents: Contents
}

struct Contents: Codable {
    let id_product: String
    let product_name: String
    let price: String
    let quantity: String
    
    enum CodingKeys: String, CodingKey {
        case id_product = "id_product"
        case product_name = "product_name"
        case price = "price"
        case quantity = "quantity"
    }
}

