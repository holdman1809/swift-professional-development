//
//  AppDelegate.swift
//  1lesson HolmogorovDmitry
//
//  Created by Дмитрий on 17/04/2019.
//  Copyright © 2019 Dmitry. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        let frame  = (UIScreen.main.bounds)
        self.window = UIWindow.init(frame: frame)
        
        let tabBarVC = TabBarViewController.init()
        self.window?.rootViewController = tabBarVC
        self.window?.makeKeyAndVisible()
        
        let requestFactory = RequestFactory()
        let auth = requestFactory.makeAuthRequestFatory()
        auth.login(userName: "Somebody", password: "mypassword") { response in
            switch response.result {
            case .success(let login):
                print(login)
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
        
        
        
        let changeData = requestFactory.makeChangeDataRequestFatory()
        changeData.changeUserData(id_user: 123, userName: "Somebody", password: "mypassword", email: "some@some.ru", gender: "m", credit_card: "9872389-2424-234224-234", bio: "This is good! I think I will switch to another language") { (response) in
            switch response.result {
            case .success(let changeData):
                print(changeData)
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
        
        let getBasket = requestFactory.makeGetBasketRequestFactory()
        getBasket.getBasket(amount: "1", countGoods: "1", id_product: "123", product_name: "MacBook Pro", price: "120000", quantity: "1") { (response) in
            switch response.result {
            case .success(let catalogData):
                print(catalogData)
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
        
        let paymentOrder = requestFactory.makePaymentOrderRequestFactory()
        paymentOrder.payOrder(order: "1", totalPrice: "120 000 ₽", id_product: "123ER67", product_name: "MacBook Pro 256GB", price: "120000", quantity: "1") { (response) in
            switch response.result {
            case .success(let data):
                print(data)
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
        
        let addToBasket = requestFactory.makeAddToBasketRequestFactory()
        addToBasket.addToBasket(id_product: 123, quantity: 1) { (response) in
            switch response.result {
            case .success(let catalogData):
                print(catalogData)
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
        
        let deleteFromBasket = requestFactory.makeDeleteFromBasketRequestFactory()
        deleteFromBasket.deleteFromBasket(id_product: 123, quantity: 1) { (response) in
            switch response.result {
            case .success(let catalogData):
                print(catalogData)
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    
        
        
        let approveReview = requestFactory.makeApproveReviewRequestFactory()
            approveReview.approveReview(id_comment: 1, completionHandler: { (response) in
            switch response.result {
            case .success(let review):
                print(review)
            case .failure(let error):
                print(error.localizedDescription)
            }
        })
        
        let removeReview = requestFactory.makeRemoveReviewRequestFactory()
            removeReview.removeReview(id_comment: 1) { (response) in
            switch response.result {
            case .success(let review):
                print(review)
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
        
        
        let logout = requestFactory.makeLogoutRequestFatory()
        logout.logout(id_user: 123) { (response) in
            switch response.result {
            case .success(let logout):
                print(logout)
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

